from django.contrib.auth import get_user_model
from rest_framework import serializers

from users.serializers import UserDetailSerializer, UserListSerializer
from .models import Patient, MedicalCard

User = get_user_model()


class PatientCreateSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.HyperlinkedRelatedField(
        many=False,
        queryset=User.objects.all(),
        view_name='user-detail'
    )

    class Meta:
        model = Patient
        fields = ['url', 'user', 'json']


class PatientDetailSerializer(serializers.HyperlinkedModelSerializer):
    user = UserDetailSerializer()

    class Meta:
        model = Patient
        fields = ['url', 'user', 'json']


class PatientListSerializer(serializers.HyperlinkedModelSerializer):
    user = UserListSerializer()

    class Meta:
        model = Patient
        fields = ['url', 'user']


class MedicalCardCreateSerializer(serializers.HyperlinkedModelSerializer):
    patient = serializers.HyperlinkedRelatedField(
        many=False,
        queryset=Patient.objects.all(),
        view_name='patient-detail'
    )

    class Meta:
        model = MedicalCard
        fields = ['url', 'id', 'date', 'patient', 'type', 'json']


class MedicalCardListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MedicalCard
        fields = ['url', 'id', 'date', 'patient', 'type']
