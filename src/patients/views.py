import django_filters
from rest_framework import viewsets

from .models import Patient, MedicalCard
from .serializers import (
    PatientCreateSerializer,
    PatientDetailSerializer,
    PatientListSerializer,
    MedicalCardCreateSerializer,
    MedicalCardListSerializer
)


class PatientFilter(django_filters.FilterSet):
    class Meta:
        model = Patient
        fields = {
            'user__last_name': ['icontains'],
            'user__first_name': ['icontains'],
            'user__middle_name': ['icontains'],
            'user__phone': ['icontains'],
        }


class PatientViewSet(viewsets.ModelViewSet):
    queryset = Patient.objects.all()
    filterset_class = PatientFilter

    def get_serializer_class(self):
        if self.action in ('create', 'partial_update', 'update'):
            return PatientCreateSerializer
        if self.action in ('list', ):
            return PatientListSerializer

        return PatientDetailSerializer


class MedicalCardFilter(django_filters.FilterSet):
    class Meta:
        model = MedicalCard
        fields = {
            'patient__user_id': ['exact'],
        }


class MedicalCardViewSet(viewsets.ModelViewSet):
    queryset = MedicalCard.objects.all()
    filterset_class = MedicalCardFilter

    def get_serializer_class(self):
        if self.action in ('list', ):
            return MedicalCardListSerializer

        return MedicalCardCreateSerializer
