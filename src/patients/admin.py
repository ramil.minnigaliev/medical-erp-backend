from django.contrib import admin

from .models import Patient, MedicalCard


@admin.register(Patient)
class PatientAdmin(admin.ModelAdmin):
    fields = ('user', 'json')
    list_display = fields


@admin.register(MedicalCard)
class MedicalCardAdmin(admin.ModelAdmin):
    fields = ('date', 'patient', 'type', 'json')
    list_display = ('id', 'date', 'patient', 'type')
