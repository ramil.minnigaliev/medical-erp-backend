from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone

User = get_user_model()


class Patient(models.Model):
    user = models.OneToOneField(
        User,
        verbose_name='пользователь',
        on_delete=models.CASCADE,
        primary_key=True
    )
    json = models.TextField('JSON', null=True, blank=True)

    class Meta:
        verbose_name = 'пациент'
        verbose_name_plural = 'пациенты'

    def __str__(self):
        return str(self.user)


class MedicalCard(models.Model):

    DENTAL_O43U = 'Медицинская карта стоматологического пациента (форма 043-у)'
    TYPE_CHOICES = [
        (DENTAL_O43U, DENTAL_O43U),
    ]

    patient = models.ForeignKey(Patient, verbose_name='пациент', on_delete=models.PROTECT)
    type = models.CharField('тип карты', choices=TYPE_CHOICES, max_length=150)
    date = models.DateField('дата создания карты', default=timezone.now)
    json = models.TextField('JSON', null=True, blank=True)

    class Meta:
        verbose_name = 'медицинская карта'
        verbose_name_plural = 'медицинские карты'

    def __str__(self):
        return f'{self.type} № {self.pk}'
