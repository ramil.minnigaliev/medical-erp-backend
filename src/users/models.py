from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

from .managers import UserManager


# TODO. gettext_lazy
class User(AbstractBaseUser, PermissionsMixin):

    MALE = 'male'
    FEMALE = 'female'
    GENDER_CHOICES = [
        (MALE, 'мужской'),
        (FEMALE, 'женский')
    ]

    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer.'),
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=False,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    last_name = models.CharField(_('last name'), max_length=150, blank=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    middle_name = models.CharField(_('middle name'), max_length=30, blank=True)
    birth_date = models.DateField('дата рождения', null=True, blank=True)
    email = models.EmailField(_('email address'), null=True, blank=True)
    phone = models.CharField('номер телефона', max_length=30, null=True, blank=True)
    gender = models.CharField('пол', choices=GENDER_CHOICES, max_length=6, null=True, blank=True)
    address = models.CharField('адрес', max_length=150, null=True, blank=True)
    json = models.TextField('JSON', null=True, blank=True)

    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'пользователь'
        verbose_name_plural = 'пользователи'

    def __str__(self):
        user_str = self.username
        if self.last_name and self.first_name and self.middle_name:
            user_str = f'{self.last_name} {self.first_name[:1]}. {self.middle_name[:1]}.'
        return f'{user_str} [{self.pk}]'

    def save(self, *args, **kwargs):
        from .logic import make_username
        if not self.pk and not self.username:
            self.username = make_username(self)
        super().save(*args, **kwargs)

    def get_full_name(self):
        full_name = f'{self.last_name} {self.first_name} {self.middle_name}'
        return full_name.strip()
