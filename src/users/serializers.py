from django.contrib.auth import get_user_model
from rest_framework import serializers
from djoser.serializers import UserSerializer as DjoserUserSerializer

User = get_user_model()


class UserDetailSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = [
            'url', 'id', 'last_name', 'first_name', 'middle_name', 'birth_date', 'phone', 'gender', 'address', 'json'
        ]


class UserListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'id', 'last_name', 'first_name', 'middle_name', 'phone']


class DjoserCurrentUserSerializer(DjoserUserSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'last_name', 'first_name', 'middle_name']
        read_only_fields = fields
