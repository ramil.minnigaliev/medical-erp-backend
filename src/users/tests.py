import pytest
from django.contrib.auth import get_user_model

User = get_user_model()


@pytest.mark.django_db
class TestUser:
    def test_create_user(self):
        username = 'foo'
        user = User.objects.create_user(username=username, password='bar')
        assert user.username == username
        assert not user.is_active
        assert not user.is_staff
        assert not user.is_superuser
        with pytest.raises(TypeError):
            User.objects.create_user()
        with pytest.raises(TypeError):
            User.objects.create_user(username='')
        with pytest.raises(ValueError):
            User.objects.create_user(username='', password='bar')

    def test_create_superuser(self):
        username = 'foo'
        admin_user = User.objects.create_superuser(username=username, password='bar')
        assert admin_user.username == username
        assert admin_user.is_active
        assert admin_user.is_staff
        assert admin_user.is_superuser
        with pytest.raises(ValueError):
            User.objects.create_superuser(
                username=username, password='bar', is_superuser=False)
