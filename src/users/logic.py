from django.contrib.auth import get_user_model

User = get_user_model()


def make_username(user):
    username = f'{user.last_name}{user.first_name}{user.middle_name}'
    count = 1
    while User.objects.filter(username=username):
        count += 1
        username = f'{username}{count}'
    return username
