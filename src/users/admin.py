from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm

User = get_user_model()


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = User
    list_display = ('username', 'last_name', 'first_name', 'middle_name', 'email', 'phone', 'address', 'gender',
                    'is_staff', 'is_active',)
    list_filter = ('is_staff', 'is_active', 'gender')
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (None, {'fields': ('last_name', 'first_name', 'middle_name', 'email', 'phone', 'address', 'gender', 'json')}),
        ('Permissions', {'fields': ('is_staff', 'is_active')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'last_name', 'first_name', 'middle_name', 'email', 'phone', 'address', 'gender',
                       'json', 'password1', 'password2', 'is_staff', 'is_active')}
        ),
    )
    search_fields = ('username', 'last_name', 'first_name', 'middle_name', 'email', 'phone', 'address')
    ordering = ('username',)
