from django.urls import include, path
from rest_framework import routers

from appointments.views import (
    AppointmentViewSet,
    ServiceViewSet,
    CabinetViewSet,
)
from doctors.views import (
    DoctorViewSet,
    DoctorSpecializationViewSet,
    SpecializationViewSet,
)
from documents.views import DocumentViewSet, DocumentTemplateViewSet
from notifications.views import NotificationViewSet
from patients.views import PatientViewSet, MedicalCardViewSet
from users.views import UserViewSet


router = routers.DefaultRouter()
router.register(r'appointments', AppointmentViewSet)
router.register(r'cabinets', CabinetViewSet)
router.register(r'doctors', DoctorViewSet)
router.register(r'doctor_specializations', DoctorSpecializationViewSet)
router.register(r'document', DocumentViewSet)
router.register(r'document_template', DocumentTemplateViewSet)
router.register(r'patients/medical_card', MedicalCardViewSet)
router.register(f'notifications', NotificationViewSet)
router.register(r'patients', PatientViewSet)
router.register(r'services', ServiceViewSet)
router.register(r'specializations', SpecializationViewSet)
router.register(r'users', UserViewSet)

urlpatterns = [
    path('api-auth/', include('rest_framework.urls')),
    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.authtoken')),
    path('', include(router.urls)),
]
