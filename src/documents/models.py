from django.contrib.auth import get_user_model
from django.db import models

from patients.models import Patient

User = get_user_model()


class DocumentTemplate(models.Model):
    name = models.CharField('название документа', max_length=150)
    file = models.FileField('файл шаблона', upload_to='documents/DocumentTemplate')

    class Meta:
        verbose_name = 'шаблон документа'
        verbose_name_plural = 'шаблоны документов'

    def __str__(self):
        return f'{self.name} [{self.pk}]'


class Document(models.Model):
    template = models.ForeignKey(DocumentTemplate, verbose_name='шаблон', on_delete=models.CASCADE)
    patient = models.ForeignKey(Patient, verbose_name='пациент', on_delete=models.CASCADE)
    file = models.FileField('файл документа', upload_to='documents/Document/%Y/%m/%d')

    class Meta:
        verbose_name = 'документ'
        verbose_name_plural = 'документы'

    def __str__(self):
        return f'{self.template} ({self.patient}) [{self.pk}]'
