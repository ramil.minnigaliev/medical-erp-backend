from django.urls import path

from . import views

app_name = 'documents'

urlpatterns = [
    path('download/<int:template_id>/<int:patient_id>/', views.download_document, name='download_document'),
]
