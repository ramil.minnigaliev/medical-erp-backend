from django.http import FileResponse
from rest_framework import viewsets

from .logic import generate_document
from .models import Document, DocumentTemplate
from .serializers import DocumentSerializer, DocumentTemplateSerializer


class DocumentViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer


class DocumentTemplateViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = DocumentTemplate.objects.all()
    serializer_class = DocumentTemplateSerializer


def download_document(request, template_id, patient_id):
    document = Document.objects.create(template_id=template_id, patient_id=patient_id)

    pdf = generate_document(template_id, patient_id)
    document.file.save(f'{template_id}-{patient_id}.pdf', pdf)

    return FileResponse(open(document.file.path, 'rb'))
