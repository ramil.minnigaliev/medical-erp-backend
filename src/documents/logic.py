import json
import tempfile
from datetime import datetime

import requests
from django.shortcuts import get_object_or_404

from documents.models import Document, DocumentTemplate
from patients.models import Patient
from odf.userfield import UserFields


class DocumentContext:
    def __init__(self, patient_id):
        self.patient = get_object_or_404(Patient, user_id=patient_id)
        self.patient_json = self.get_patient_json()
        self.context = {
            'Документ.Номер': self.get_document_number(),
            'Документ.Дата': self.get_document_date(),
            'Пациент.ФамилияИмяОтчество': self.get_patient_full_name(),
            'Пациент.ДатаРождения': self.get_patient_birth_date(),
            'Пациент.Телефон': self.get_patient_phone(),
            'Пациент.Адрес': self.get_patient_address(),
            'Пациент.Паспорт.Серия': self.get_json_field(self.patient_json, 'Паспорт', 'Серия'),
            'Пациент.Паспорт.Номер': self.get_json_field(self.patient_json, 'Паспорт', 'Номер'),
            'Пациент.Паспорт.Выдан': self.get_json_field(self.patient_json, 'Паспорт', 'Выдан'),
            'Пациент.Паспорт.Дата': self.get_json_field(self.patient_json, 'Паспорт', 'Дата'),
            'Пациент.Паспорт.Код': self.get_json_field(self.patient_json, 'Паспорт', 'Код подразделения'),
            'Пациент.ИНН': self.get_json_field(self.patient_json, 'ИНН'),
            'Пациент.СНИЛС': self.get_json_field(self.patient_json, 'СНИЛС'),
        }

    def get_json_field(self, obj, *args):
        if not args[0] in obj:
            return ''
        left_args = args[1:]
        if left_args:
            return self.get_json_field(obj[args[0]], *left_args)
        return obj[args[0]]

    def get_patient_json(self):
        return json.loads(self.patient.user.json)

    def get_context(self):
        return self.context

    @staticmethod
    def get_document_number():
        return Document.objects.order_by('id').last().id

    @staticmethod
    def get_document_date():
        return f'{datetime.today().strftime("%d.%m.%Y")} г.'

    def get_patient_full_name(self):
        return self.patient.user.get_full_name()

    def get_patient_birth_date(self):
        if self.patient.user.birth_date:
            return f'{self.patient.user.birth_date.strftime("%d.%m.%Y")} г.'
        return '-'

    def get_patient_phone(self):
        if self.patient.user.phone:
            return self.patient.user.phone
        return '-'

    def get_patient_address(self):
        if self.patient.user.address:
            return self.patient.user.address
        return '-'


def generate_document(template_id, patient_id):
    template = get_object_or_404(DocumentTemplate, id=template_id)

    with tempfile.NamedTemporaryFile() as output:

        context = DocumentContext(patient_id).get_context()

        fields = UserFields(template.file.path, output.name)
        fields.update(context)

        pdf = get_pdf(output.name)

    return pdf


def get_pdf(path):
    with open(path, 'rb') as f:
        r = requests.post('http://lo2pdf:6000', files={
            'upload_file': f
        }, stream=True)

        if r.status_code == 200:
            r.raw.decode_content = True
            return r.raw
