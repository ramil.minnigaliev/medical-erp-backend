from rest_framework import serializers

from patients.serializers import PatientDetailSerializer
from .models import Document, DocumentTemplate


class DocumentSerializer(serializers.HyperlinkedModelSerializer):
    patient = PatientDetailSerializer()

    class Meta:
        model = Document
        fields = ['url', 'id', 'template', 'patient', 'file']


class DocumentTemplateSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = DocumentTemplate
        fields = ['url', 'id', 'name', 'file']
