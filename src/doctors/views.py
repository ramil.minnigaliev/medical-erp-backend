from rest_framework import viewsets

from .models import Doctor, DoctorSpecialization, Specialization
from .serializers import (
    DoctorCreateSerializer,
    DoctorDetailSerializer,
    DoctorSpecializationCreateSerializer,
    DoctorSpecializationDetailSerializer,
    SpecializationSerializer,
)


class DoctorViewSet(viewsets.ModelViewSet):
    queryset = Doctor.objects.all()

    def get_serializer_class(self):
        if self.action in ('create', 'partial_update', 'update'):
            return DoctorCreateSerializer
        return DoctorDetailSerializer


class SpecializationViewSet(viewsets.ModelViewSet):
    queryset = Specialization.objects.all()
    serializer_class = SpecializationSerializer


class DoctorSpecializationViewSet(viewsets.ModelViewSet):
    queryset = DoctorSpecialization.objects.all()

    def get_serializer_class(self):
        if self.action in ('create', 'partial_update', 'update'):
            return DoctorSpecializationCreateSerializer
        return DoctorSpecializationDetailSerializer
