from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class Specialization(models.Model):
    name = models.CharField('наименование', max_length=150)

    class Meta:
        verbose_name = 'специализация'
        verbose_name_plural = 'специализации'

    def __str__(self):
        return self.name


class Doctor(models.Model):
    user = models.OneToOneField(User, verbose_name='пользователь', on_delete=models.CASCADE,
                                primary_key=True)
    position = models.CharField('должность', max_length=150, default='', blank=True)
    html_classes = models.CharField('стили HTML', max_length=255, default='', blank=True)

    class Meta:
        verbose_name = 'врач'
        verbose_name_plural = 'врачи'
        ordering = ['user__last_name']

    def __str__(self):
        return f'{self.position} {str(self.user)}'


class DoctorSpecialization(models.Model):
    doctor = models.ForeignKey('doctors.doctor', verbose_name='врач', on_delete=models.CASCADE)
    specialization = models.ForeignKey('doctors.specialization', verbose_name='специализация', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Специализация врача'
        verbose_name_plural = 'Специализации врачей'
        unique_together = [['doctor', 'specialization']]

    def __str__(self):
        return f'{self.doctor} - {self.specialization}'
