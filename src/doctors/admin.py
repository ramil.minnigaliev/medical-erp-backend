from django.contrib import admin

from .models import (
    Doctor,
    DoctorSpecialization,
    Specialization
)


@admin.register(Doctor)
class DoctorAdmin(admin.ModelAdmin):
    fields = ('user', 'position', 'html_classes')
    list_display = fields


@admin.register(DoctorSpecialization)
class DoctorSpecializationAdmin(admin.ModelAdmin):
    fields = ('doctor', 'specialization',)
    list_display = fields


@admin.register(Specialization)
class SpecializationAdmin(admin.ModelAdmin):
    fields = ('name',)
    list_display = fields
