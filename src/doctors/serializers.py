from django.contrib.auth import get_user_model
from rest_framework import serializers

from users.serializers import UserDetailSerializer
from .models import Doctor, DoctorSpecialization, Specialization

User = get_user_model()


class SpecializationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Specialization
        fields = ['url', 'id', 'name']


class DoctorCreateSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.HyperlinkedRelatedField(
        many=False,
        queryset=User.objects.all(),
        view_name='user-detail'
    )

    class Meta:
        model = Doctor
        fields = ['url', 'user', 'position', 'html_classes']


class DoctorDetailSerializer(serializers.HyperlinkedModelSerializer):
    user = UserDetailSerializer()

    class Meta:
        model = Doctor
        fields = ['url', 'user', 'position', 'html_classes']


class DoctorSpecializationCreateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DoctorSpecialization
        fields = ['url', 'id', 'doctor', 'specialization']


class DoctorSpecializationDetailSerializer(serializers.HyperlinkedModelSerializer):
    doctor = DoctorDetailSerializer()
    specialization = SpecializationSerializer()

    class Meta:
        model = DoctorSpecialization
        fields = ['url', 'id', 'doctor', 'specialization']
