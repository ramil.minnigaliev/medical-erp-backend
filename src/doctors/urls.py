from django.urls import include, path
from rest_framework import routers

from . import views


router = routers.DefaultRouter()
router.register(r'doctors', views.DoctorViewSet)
router.register(r'specializations', views.SpecializationViewSet)
router.register(r'doctor_specializations', views.DoctorSpecializationViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
