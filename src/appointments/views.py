import datetime

import django_filters
from django.utils import timezone
from rest_framework import viewsets, status
from rest_framework.response import Response

from .models import Appointment, Cabinet, Service
from .serializers import (
    AppointmentCreateSerializer,
    AppointmentDetailSerializer,
    CabinetSerializer,
    ServiceSerializer
)


class AppointmentFilter(django_filters.FilterSet):
    class Meta:
        model = Appointment
        fields = {
            'start_time': ['lt', 'gt'],
        }


class AppointmentViewSet(viewsets.ModelViewSet):
    queryset = Appointment.objects.all()
    filterset_class = AppointmentFilter

    def get_serializer_class(self):
        if self.action in ('create', 'partial_update', 'update'):
            return AppointmentCreateSerializer
        return AppointmentDetailSerializer

    def check_time_limit(self):
        """Return True if an appointment.start_time in the past"""
        appointment = self.get_object()
        limit = timezone.now() - datetime.timedelta(hours=12)
        return appointment.start_time < limit

    def update(self, request, *args, **kwargs):
        """Forbid update of an appointment after some time"""
        if self.check_time_limit():
            return Response(status=status.HTTP_403_FORBIDDEN)
        return super().update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        """Forbid delete of an appointment after some time"""
        if self.check_time_limit():
            return Response(status=status.HTTP_403_FORBIDDEN)
        return super().destroy(request, *args, **kwargs)


class CabinetViewSet(viewsets.ModelViewSet):
    queryset = Cabinet.objects.all()
    serializer_class = CabinetSerializer


class ServiceViewSet(viewsets.ModelViewSet):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer
