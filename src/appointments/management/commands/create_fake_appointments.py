from django.core.management.base import BaseCommand
from django.utils.timezone import make_aware


class Command(BaseCommand):
    help = 'Create fake appointments'

    def add_arguments(self, parser):
        parser.add_argument('count', type=int, help='Количество создаваемых записей')
        parser.add_argument('date', type=str, help='Дата в формате ГГГГ-ММ-ДД')

    def handle(self, *args, **options):
        import random
        from appointments.models import Appointment, Service
        from doctors.models import Doctor
        from patients.models import Patient
        from ._random_period import random_period

        services = Service.objects.all()
        doctors = Doctor.objects.all()
        patients = Patient.objects.all()

        count = options['count']
        date = options['date']

        for _ in range(count):
            start_time, end_time = random_period(f'{date} 09:00', f'{date} 17:00')

            Appointment.objects.create(
                service=random.choice(services),
                doctor=random.choice(doctors),
                patient=random.choice(patients),
                start_time=make_aware(start_time),
                end_time=make_aware(end_time)
            )
