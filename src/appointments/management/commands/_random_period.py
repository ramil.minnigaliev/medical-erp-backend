from datetime import datetime
import random
import time


def random_period(start, end, fmt='%Y-%m-%d %H:%M', shift=30*60):
    stime = time.mktime(time.strptime(start, fmt))
    etime = time.mktime(time.strptime(end, fmt))

    sptime = stime + random.random() * (etime - stime)
    eptime = sptime + shift

    return datetime.fromtimestamp(sptime), datetime.fromtimestamp(eptime)
