from rest_framework import serializers

from doctors.serializers import DoctorDetailSerializer
from patients.serializers import PatientDetailSerializer
from .models import Appointment, Cabinet, Service


class CabinetSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Cabinet
        fields = ['url', 'id', 'name', 'html_classes']


class ServiceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Service
        fields = ['url', 'id', 'name']


class AppointmentCreateSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Appointment
        fields = ['url', 'id', 'doctor', 'cabinet', 'patient', 'service', 'start_time', 'end_time']


class AppointmentDetailSerializer(serializers.ModelSerializer):
    doctor = DoctorDetailSerializer()
    patient = PatientDetailSerializer()
    service = ServiceSerializer()
    cabinet = CabinetSerializer()

    class Meta:
        model = Appointment
        fields = ['url', 'id', 'doctor', 'cabinet', 'patient', 'service', 'start_time', 'end_time', 'notification']
