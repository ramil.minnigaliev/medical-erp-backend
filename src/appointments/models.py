from django.db import models


class Cabinet(models.Model):
    name = models.CharField('кабинет', max_length=50)
    html_classes = models.CharField('стили HTML', max_length=255, default='', blank=True)

    class Meta:
        verbose_name = 'кабинет'
        verbose_name_plural = 'кабинеты'

    def __str__(self):
        return self.name


class Service(models.Model):
    name = models.CharField('наименование', max_length=150)

    class Meta:
        verbose_name = 'услуга'
        verbose_name_plural = 'услуги'

    def __str__(self):
        return self.name


class Appointment(models.Model):
    doctor = models.ForeignKey('doctors.doctor', verbose_name='врач', on_delete=models.PROTECT)
    cabinet = models.ForeignKey('appointments.cabinet', verbose_name='кабинет', on_delete=models.PROTECT, null=True)
    patient = models.ForeignKey('patients.patient', verbose_name='пациент', on_delete=models.PROTECT)
    service = models.ForeignKey('appointments.service', verbose_name='услуга', on_delete=models.PROTECT)
    start_time = models.DateTimeField('время начала')
    end_time = models.DateTimeField('время окончания')

    class Meta:
        verbose_name = 'запись к врачу'
        verbose_name_plural = 'записи к врачам'
        ordering = ['start_time']

    def __str__(self):
        return f'{self.service} у {self.doctor} в {self.start_time}'
