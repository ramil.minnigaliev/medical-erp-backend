from django.contrib import admin

from .models import (
    Appointment,
    Service,
    Cabinet
)


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    fields = ('doctor', 'cabinet', 'patient', 'service', 'start_time', 'end_time')
    list_display = fields
    ordering = ['-start_time']


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    fields = ('name', )
    list_display = fields


@admin.register(Cabinet)
class CabinetAdmin(admin.ModelAdmin):
    fields = ('name', 'html_classes')
    list_display = fields
