from django.db import models


class Notification(models.Model):

    INFO = 'info'
    CALL = 'call'

    TYPE_CHOICES = [
        (INFO, 'Инфо'),
        (CALL, 'Звонок')
    ]

    date = models.DateTimeField('Когда напомнить')
    type = models.CharField(
        'Тип напоминания',
        max_length=32,
        choices=TYPE_CHOICES,
        default=INFO
    )
    is_active = models.BooleanField(
        'Активно',
        default=True
    )
    text = models.TextField(
        'Текст напоминания',
        default='',
        null=True,
        blank=True
    )
    appointment = models.OneToOneField(
        'appointments.appointment',
        verbose_name='Запись на приём',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    class Meta:
        verbose_name = 'напоминание'
        verbose_name_plural = 'напоминания'
        ordering = ['date']

    def __str__(self):
        return f'{self.date}: {self.type}'
