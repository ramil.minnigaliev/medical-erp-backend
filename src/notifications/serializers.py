from rest_framework import serializers

from appointments.serializers import AppointmentDetailSerializer
from .models import Notification


class NotificationCreateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Notification
        fields = ['url', 'id', 'date', 'type', 'is_active', 'text', 'appointment']


class NotificationDetailSerializer(serializers.HyperlinkedModelSerializer):
    appointment = AppointmentDetailSerializer()

    class Meta:
        model = Notification
        fields = ['url', 'id', 'date', 'type', 'is_active', 'text', 'appointment']
