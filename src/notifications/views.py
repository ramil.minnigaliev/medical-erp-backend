import django_filters
from rest_framework import viewsets

from .models import Notification
from .serializers import NotificationCreateSerializer, NotificationDetailSerializer


class NotificationFilter(django_filters.FilterSet):
    class Meta:
        model = Notification
        fields = {
            'date': ['lt', 'gt'],
            'type': ['exact'],
            'is_active': ['exact'],
        }


class NotificationViewSet(viewsets.ModelViewSet):
    queryset = Notification.objects.all()
    filterset_class = NotificationFilter

    def get_serializer_class(self):
        if self.action in ('create', 'partial_update', 'update'):
            return NotificationCreateSerializer
        return NotificationDetailSerializer
