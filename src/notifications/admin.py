from django.contrib import admin

from .models import Notification


@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
    fields = ('date', 'type', 'is_active', 'text')
    list_display = fields
    ordering = ['-date']
