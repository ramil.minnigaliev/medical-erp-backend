# Информационная система для медицинских клиник
## Запуск проекта для разработки
1. Устанавливаем:
    ```
    docker-compose, virtualenv, python3-wheel
    ```
1. В корне проекта запускаем сборку
    ```
    docker-compose build
    ```
1. Запускаем контейнеры
    ```
    docker-compose up
    ```
1. Создаем виртуальное окружение python3. И далее работаем только в нем.
1. Для загрузки переменных окружения нужно добавить в конец bin/activate виртуального окружения:
    ```bash
    # Load env file
    set -a
    . ~/dev/src/medical-erp-backend/config/dev.env
    set +a
    ```
1. Устанавливаем зависимости в проект
    ```
    pip install -r requirements.in
    pip install -r requirements.dev.in
    ```
1. В /etc/hosts добавим db
    ```
    127.0.0.1	localhost db
    ```
1. Создаем и применяем миграции
    ```
    ./manage.py migrate
    ```
1. Загружаем фикстуры
    ```
    ./manage.py loaddata fixtures/*
    ```
1. Все готово, заходим на localhost.

## Сброс миграций
```shell script
bash utils/reset_migrations.sh
```

## Создание демо-записей
```shell script
./manage.py create_fake_appointments 10 2020-04-25
```
